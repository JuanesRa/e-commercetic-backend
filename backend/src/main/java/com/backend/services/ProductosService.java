package com.backend.services;

import java.util.ArrayList;

import com.backend.models.ProductosModel;
import com.backend.repositories.ProductosRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductosService {
    @Autowired
    ProductosRepository usarProductosRepository;

    public ArrayList<ProductosModel> obtenerProductos() {
        return (ArrayList<ProductosModel>) usarProductosRepository.findAll();
    }

    public ProductosModel guardarProductos(ProductosModel productos) {
        return usarProductosRepository.save(productos);
    }
}
