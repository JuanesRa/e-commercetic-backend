package com.backend.repositories;

import com.backend.models.ProductosModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductosRepository extends CrudRepository<ProductosModel, Long> {

}
