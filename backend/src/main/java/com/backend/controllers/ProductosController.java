package com.backend.controllers;

import java.util.ArrayList;

import com.backend.models.ProductosModel;
import com.backend.services.ProductosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/producto")
public class ProductosController {
    @Autowired
    ProductosService productosService;

    @GetMapping
    public ArrayList<ProductosModel> obtenerProductos() {
        return productosService.obtenerProductos();
    }

    @PostMapping
    public ProductosModel guardarProductos(@RequestBody ProductosModel productos) {
        return this.productosService.guardarProductos(productos);
    }
}
